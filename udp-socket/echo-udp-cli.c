#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>


#define LINE_MAX_LEN  2048

void udp_echo_cli(FILE *fp, int sockfd, const struct sockaddr *pservaddr, socklen_t addrlen)
{
	ssize_t n;
	char sendline[LINE_MAX_LEN], recvline[LINE_MAX_LEN + 1];

	while(fgets(sendline, LINE_MAX_LEN, fp) != NULL)
	{
		n = sendto(sockfd, sendline, strlen(sendline), 0, pservaddr, addrlen);
		if(n < 0)
		{
			perror("sendto error");
			exit(EXIT_FAILURE);
		}

		n = recvfrom(sockfd, recvline, LINE_MAX_LEN, 0, NULL, NULL);
		if(n < 0)
		{
			perror("recvfrom error");
			exit(EXIT_FAILURE);
		}

		recvline[n] = '\0';
		fputs(recvline, stdout);
	}
}

int main(int argc, char **argv)
{
	int sockfd;
	struct sockaddr_in servaddr;
	int port;

	if(argc != 3)
	{
		printf("usage: %s <A.B.C.D> <port>\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if(sockfd < 0)
	{
		perror("socket created error");
		exit(EXIT_FAILURE);
	}

	port = atoi(argv[2]);
	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(port);
	if(inet_pton(AF_INET, argv[1], &servaddr.sin_addr.s_addr) != 1)
	{
		perror("inet_pton error");
		exit(EXIT_FAILURE);
	}

	udp_echo_cli(stdin, sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr));
	exit(EXIT_SUCCESS);
}
