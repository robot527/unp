#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>


#define SERV_PORT     9300
#define LINE_MAX_LEN  2048

void udp_echo(int sockfd, struct sockaddr *paddr, socklen_t addrlen)
{
	ssize_t n;
	char buf[LINE_MAX_LEN];

	while(1)
	{
		n = recvfrom(sockfd, buf, LINE_MAX_LEN, 0, paddr, &addrlen);
		if(n < 0)
		{
			perror("recvfrom error");
			exit(EXIT_FAILURE);
		}
		printf("recv %d bytes\n", n);
		n = sendto(sockfd, buf, n, 0, paddr, addrlen);
		if(n < 0)
		{
			perror("sendto error");
			exit(EXIT_FAILURE);
		}
	}
}

int main(int argc, char **argv)
{
	int sockfd;
	struct sockaddr_in servaddr, cliaddr;

	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if(sockfd < 0)
	{
		perror("socket created error");
		exit(EXIT_FAILURE);
	}

	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(SERV_PORT);

	if(bind(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0)
	{
		perror("bind error");
		exit(EXIT_FAILURE);
	}

	udp_echo(sockfd, (struct sockaddr *)&cliaddr, sizeof(cliaddr));
	exit(EXIT_SUCCESS);
}
