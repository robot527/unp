#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define SERV_PORT 9300
#define LINE_MAX_LEN 2048

#define MAX(a, b) ((a) > (b) ? (a) : (b))


/* 采用select的客户端消息处理函数 */
void str_cli(FILE* fp, int sockfd)
{
	int maxfd;
	fd_set rset;
	char buf[LINE_MAX_LEN];
	ssize_t n;
	int stdineof = 0;

	FD_ZERO(&rset);
	while(1)
	{
		/* 将文件描述符和套接字描述符添加到rset描述符集 */
		if (stdineof == 0)
			FD_SET(fileno(fp), &rset);	
		FD_SET(sockfd, &rset);
		maxfd = MAX(fileno(fp), sockfd) + 1;
		select(maxfd, &rset, NULL, NULL, NULL);

		if(FD_ISSET(sockfd, &rset))
		{/* socket is readable */
			if((n = read(sockfd, buf, LINE_MAX_LEN)) == 0)
			{
				if (stdineof == 1)
					return; /* normal termination */
				else
				{
					printf("str_cli: server terminated prematurely.\n");
					exit(EXIT_FAILURE);
				}	
			}
			buf[n] = '\0';
			write(fileno(stdout), buf, n);
			printf("\n");
		}

		if(FD_ISSET(fileno(fp), &rset))
		{/* input is readable */
			if((n = read(fileno(fp), buf, LINE_MAX_LEN)) == 0)
			{
				printf("read nothing~\n");
				stdineof = 1;
				shutdown(sockfd, SHUT_WR); /* send FIN */
				FD_CLR(fileno(fp), &rset); /* 从集合rset移除文件描述符fileno(fp) */
				continue;
			}
			buf[n] = '\0';
			write(sockfd, buf, n);
		}
	}
}

int main(int argc, char **argv)
{
    int sockfd;
    struct sockaddr_in servaddr;

    /* 判断是否为合法输入 */
    if(argc != 2)
    {
        printf("usage: %s <A.B.C.D>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    /* step 1: 创建套接字 */
    if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("socket error");
        exit(EXIT_FAILURE);
    }

    /* step 2: 设置连接服务器地址结构 */
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(SERV_PORT);
    if(inet_pton(AF_INET, argv[1], &servaddr.sin_addr) < 0)
    {
        printf("inet_pton error for %s\n", argv[1]);
        exit(EXIT_FAILURE);
    }

    /* step 3: 发送连接服务器请求 */
    if(connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0)
    {
        perror("connected error");
        exit(EXIT_FAILURE);
    }

	/* step 4: 发送文本，输出回显 */
	str_cli(stdin, sockfd);
	exit(EXIT_SUCCESS);
}
