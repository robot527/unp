
[NAME](#name) | [SYNOPSIS](#synopsis) | [DESCRIPTION](#description) | [RETURN VALUE](#return-value) | [ERRORS](#errors) | [VERSIONS](#versions) | [CONFORMING TO](#conforming-to) | [NOTES](#notes) | [BUGS](#bugs) | [EXAMPLE](#exmaple) | [SEE ALSO](#see-also) | [COLOPHON](#colophon)


```
SELECT(2)                 Linux 程序员手册                         SELECT(2)
```

### NAME   

```
       select,  pselect, FD_CLR, FD_ISSET, FD_SET, FD_ZERO - 同步 I/O  复用
```

### SYNOPSIS  说明书         [top](#name)

```
       /* 按照 POSIX.1-2001, POSIX.1-2008 */
       #include <sys/select.h>

       /* 按照较早的标准 */
       #include <sys/time.h>
       #include <sys/types.h>
       #include <unistd.h>

       int select(int nfds, fd_set *readfds, fd_set *writefds,
                  fd_set *exceptfds, struct timeval *timeout);

       void FD_CLR(int fd, fd_set *set);
       int  FD_ISSET(int fd, fd_set *set);
       void FD_SET(int fd, fd_set *set);
       void FD_ZERO(fd_set *set);

       #include <sys/select.h>

       int pselect(int nfds, fd_set *readfds, fd_set *writefds,
                   fd_set *exceptfds, const struct timespec *timeout,
                   const sigset_t *sigmask);
```

对于 glibc 特性测试宏要求 (参见 [feature_test_macros(7)][1]):

	pselect(): _POSIX_C_SOURCE >= 200112L

### DESCRIPTION  描述         [top](#name)

select() 和 pselect() 允许程序监视多个文件描述符， 等待直到文件描述符中的一个或多个变为对于某类 I/O 操作（例如，可能的输入）"ready"。  
如果可以执行相应的 I/O  操作，则认为文件描述符已准备就绪 (例如，不阻塞的 [read(2)][2]，或足够小的 [write(2)][3])。  

select() 只能监视数量小于 FD_SETSIZE 的文件描述符 ; [poll(2)][4] 没有这个限制。 参见 BUGS。  

select() 和 pselect() 的操作是相同的，除了这三个区别之外：

(i)    select() 使用超时，即 *struct timeval* （具有秒和微秒）， 而 pselect() 使用 *struct timespec* (具有秒和纳秒)。

(ii)   select() 可能更新 *timeout* 参数以指示剩余多少时间。  pselect() 不改变这个参数。

(iii)  select() 没有参数 *sigmask*， 并表现为使用 NULL 作为 *sigmask* 调用的 pselect()。

三个独立的文件描述符集合被监视。 在 *readfds* 中列出的那些将被监视以查看是否有字符可用于读取（更准确地说，查看读取是否不会阻塞；  
特别是，文件描述符也在文件结尾处准备就绪）， *writefds* 中的那些将被监视以查看空间是否可用于写入（虽然大块写仍可能阻塞）， 而  
*exceptfds* 中的那些将被监视异常。在退出时，这些集合被适当地修改以指示哪些文件描述符实际上改变了状态。 如果没有要观察对应类事件的  
文件描述符，则三个文件描述符集合中的每一个可以被指定为 NULL。  

提供了四个宏来操作集合。 FD_ZERO() 清除一个集合。 FD_SET() 和 FD_CLR() 分别地从一个集合添加和移除一个给定的文件描述符。  
FD_ISSET() 测试以查看文件描述符是否是集合的一部分（是否可以读写）; 这在 select() 返回后是有用的。  

*nfds* 是在三个集合中的任何集合中的编号最大的文件描述符，再加一。  

*timeout* 参数指定 select() 应阻塞等待文件描述符准备就绪的时间间隔。调用将阻塞，直到：

	*  文件描述符变为就绪 ;

	*  调用被信号处理器中断 ; 或者

	*  超时到期。

注意，*timeout* 间隔将向上舍入到系统时钟粒度，并且内核调度延迟意味着阻塞间隔可能少量超出。  
如果 *timeval* 结构体的两个字段都为零，则 select() 立即返回。  (这对于轮询是有用的。)  
如果 *timeout* 为 NULL(无超时), select() 可以无限期地阻塞。  

*sigmask* 是指向信号掩码的指针 (参见 [sigprocmask(2)][5]);  
如果它不是 NULL, 那么 pselect() 首先将当前信号掩码替换为 *sigmask* 指向的那个掩码，  
然后执行 "select" 函数, 然后恢复原始信号掩码。  

除了 *timeout* 参数的精度差异之外, 下面的 pselect() 调用 :

		ready = pselect(nfds, &readfds, &writefds, &exceptfds,
                           timeout, &sigmask);

等效于*原子地*执行以下调用 :

		sigset_t origmask;

		pthread_sigmask(SIG_SETMASK, &sigmask, &origmask);
		ready = select(nfds, &readfds, &writefds, &exceptfds, timeout);
		pthread_sigmask(SIG_SETMASK, &origmask, NULL);

之所以需要 pselect() 是因为如果想要等待信号或文件描述符准备就绪，则需要进行原子测试以防止竞态条件。  
(假设信号处理程序设置一个全局标志并返回。 然后，如果信号刚好在测试之后但刚好在调用之前到达，那么 select()  
的调用后接着的对该全局标志的测试可能无限期地挂起。相比之下, pselect() 允许先阻塞信号, 处理已经进来的信号,  
然后用期望的 *sigmask* 参数调用 pselect(), 避免竞态。)  

**超时**  
涉及的时间结构在 *<sys/time.h>* 中定义，看起来像：

           struct timeval {
               long    tv_sec;         /* 秒数 */
               long    tv_usec;        /* 微秒数 */
           };

		和

           struct timespec {
               long    tv_sec;         /* 秒数 */
               long    tv_nsec;        /* 纳秒数 */
           };

		(无论如何，请参见下面的 POSIX.1 版本。)

一些代码以三个空集合、为零的 *nfds* 和一个非空的 *timeout* 调用 select() 来作为一个亚秒精度的相当好移  
植的睡眠方式。  

在 Linux 上， select() 修改 *timeout* 以反映未睡眠的时间量；大多数其他实现不这样做。(POSIX.1 允许任一行为。)  
当将读取 *timeout* 的 Linux 代码移植到其他操作系统时，以及当将代码移植到 Linux，在不重新初始化它的情况下，  
在一个循环中重用 *struct timeval* 用于多个 select() 时会导致问题。 在 select() 返回后任务 *timeout* 未定义。  

### RETURN VALUE  返回值     [top](#name)

如果成功， select() 和 pselect() 返回包含在三个返回的描述符集中的文件描述符的数量 (即在 *readfds*、  
*writefds*、*exceptfds* 中设置的位的总数) 如果在任何令人关注的事情发生之前超时时间已到它可能是零。  
如果失败，返回-1, 且设置 [errno][11] 以指示错误；文件描述符集是未修改的，而且 timeout 变得不确定。  

### ERRORS  错误     [top](#name)

**EBADF**  在其中一个集合中给出了无效的文件描述符。
	  (或许一个已经关闭的文件描述符，或者在其中一个上发生了错误。)

**EINTR**  一个信号被捕获；参见 [signal(7)][6]。

**EINVAL** *nfds* 是负数或超过 RLIMIT_NOFILE 资源限制 (参见 [getrlimit(2)][7])。

**EINVAL** *timeout* 内包含的值无效。

**ENOMEM** 无法为内部表分配内存。

### VERSIONS  版本     [top](#name)

pselect() 在 kernel 2.6.16 被加入 Linux 。  
在此之前， pselect() 被模拟到了 glibc 里 (但请参看 BUGS)。  

### CONFORMING TO  遵循     [top](#name)

select() 遵循 POSIX.1-2001, POSIX.1-2008, 和 4.4 BSD (select() 首次出现在 4.2 BSD)。  
通常可移植到/支持 BSD 套接字层克隆的非 BSD 系统 (包括 System V 变种)。  
但是，请注意，System V 变种通常在退出之前设置超时变量，但是 BSD 变种不会。  

pselect() 在 POSIX.1 g, POSIX.1-2001 和 POSIX.1-2008 中定义。  

### NOTES  注意事项    [top](#name)

*fd_set* 是固定大小的缓冲区。 使用负值或等于或大于 FD_SETSIZE 的 fd 值执行 FD_CLR() 或 FD_SET()  
将导致未定义的行为。此外，POSIX 要求 fd 是一个有效的文件描述符。  

在其他一些 UNIX 系统上，如果系统无法分配内核内部资源，select() 可能会失败，出现错误 EAGAIN，  
而不是像 Linux 那样使用 ENOMEM。POSIX 为 [poll(2)][4] 指定此错误，但不为 select() 指定此错误。  
可移植程序可能希望检查 EAGAIN 并循环，就像 EINTR。  

关于所涉及的类型，典型情况是 *timeval* 结构的两个字段类型为 *long* (如上所示)，  
且该结构体在 <sys/time.h> 中定义。  
POSIX.1 的情况是

		struct timeval {
			time_t         tv_sec;     /* 秒数 */
			suseconds_t    tv_usec;    /* 微秒数 */
		};

其中结构在 <sys/select.h> 中定义，数据类型 time_t 和 suseconds_t 在 <sys/types.h> 中定义。  

关于原型，典型情况是对于 select() 应该包括 <time.h>。  
POSIX.1 的情况是对于 select() 和 pselect() 应该包括 <sys/select.h>。  

在 glibc 2.0 下，<sys/select.h> 给出了 pselect() 的错误原型。  
在 glibc 2.1 到 2.2.1 下，当定义 **_GNU_SOURCE** 时，它提供 pselect()。  
从 glibc 2.2.2 开始，要求如 SYNOPSIS 所示.  

**多线程应用程序**  
如果被 select() 监视的文件描述符在另一个线程中关闭，则结果未指定。  
在某些 UNIX 系统上，select() 解除阻塞并返回，并指示文件描述符已就绪  
(随后的 I/O 操作可能会失败并出错，除非在 select() 返回和 I/O 操作执行之间的时间重新打开了另一个文件描述符)。  
在 Linux （和一些其他系统）上，关闭另一个线程中的文件描述符对 select() 没有影响。  
总之，在这种情况下依赖于特定行为的任何应用程序必须被认为有 bug 的。  

**C library/kernel 差异**  
Linux 内核允许任意大小的文件描述符集，从 *nfds* 的值确定要检查的集合的长度。  
但是，在 glibc 实现中， *fd_set* 类型的大小是固定的。参见 BUGS。  

此页面中描述的 pselect() 接口由 glibc 实现。  
底层 Linux 系统调用名为 pselect6()。这个系统调用与 glibc 封装的函数有一些不同的行为。  

Linux pselect6() 系统调用修改其 timeout 参数。  
但是，glibc 封装的函数通过对传递给系统调用的 timeout 参数使用本地变量来隐藏此行为。  
因此，glibc 的 pselect() 函数不会修改其 timeout 参数；这是 POSIX.1-2001 要求的行为。  

pselect6() 系统调用的最后一个参数不是一个 sigset_t * 指针，而是一个以下形式的结构体：

		struct {
			const sigset_t *ss;     /* 指向信号集合的指针 */
			size_t          ss_len; /* 'ss' 指向的对象的大小（以字节为单位） */
		};

这允许系统调用获得指向信号集的指针及其大小，  
同时考虑了大多数体系结构支持最多 6 个参数的系统调用的事实。  

### BUGS         [top](#name)

POSIX 允许实现在文件描述符集合中可以指定的文件描述符的范围上定义通过常量 FD_SETSIZE 通告的上限。  
Linux 内核不施加固定限制，但是 glibc 实现使 *fd_set* 为固定大小类型，FD_SETSIZE 定义为 1024，FD_*() 宏根据该限制操作。  
要监视多于 1023 的文件描述符，请改用 [poll(2)][4] 。  

Glibc 2.0 提供了一个不带 sigmask 参数的 pselect() 版本。  

从版本 2.1 开始，glibc 提供了使用 [sigprocmask(2)][5] 和 select() 实现的 pselect() 的仿真。  
对于 pselect() 旨在防止非常竞态条件这个实现仍然脆弱。  
现代版本的 glibc 使用内核提供的（无竞态的） pselect() 系统调用。  

在缺乏 pselect() 的系统上，可以使用 [self-pipe trick][8] 实现可靠（和移植性更好）的信号捕获。  
在这种技术中，信号处理程序将一个字节写入一个管道，其另一端由主程序中的 select() 监视。  
(为了避免在写入可能已满的管道或从可能为空的管道读取时可能的阻塞，当从管道读取和写入管道时将使用非阻塞 I/O。)  

在 Linux 下，select() 可能会将套接字文件描述符报告为“准备读取”，但还是有后续的读取块。  
这可能例如在数据已经到达但是在检查时具有错误的校验和并且被丢弃时发生。  
可能存在其中一个文件描述符被伪造地报告为准备好的其他情况。  
因此，在不应阻塞的套接字上使用 **O_NONBLOCK** 可能更安全。  

在 Linux 上，如果调用被信号处理程序中断（即返回 **EINTR** 错误），select() 也会修改 *timeout*。  
这是 POSIX.1 不允许的。Linux pselect() 系统调用具有相同的行为，但 glibc 封装通过将 *timeout*  
内部复制到本地变量并将该变量传递到系统调用来隐藏此行为。  

### EXAMPLE  范例     [top](#name)

```
	#include <stdio.h>
	#include <stdlib.h>
	#include <sys/time.h>
	#include <sys/types.h>
	#include <unistd.h>

	int
	main(void)
	{
		fd_set rfds;
		struct timeval tv;
		int retval;

		/* 监视 stdin (fd 0) 何时有输入。 */

		FD_ZERO(&rfds);
		FD_SET(0, &rfds);

		/* 等待五秒钟。 */

		tv.tv_sec = 5;
		tv.tv_usec = 0;

		retval = select(1, &rfds, NULL, NULL, &tv);
		/* 此时不要信任 tv 的值 ! */

		if (retval == -1)
			perror("select()");
		else if (retval)
			printf("Data is available now.\n");
			/* FD_ISSET(0, &rfds) 会为真。 */
		else
			printf("No data within five seconds.\n");

		exit(EXIT_SUCCESS);
	}
```

### SEE ALSO  另请参阅   [top](#name)

[accept(2)][9], [connect(2)][10], [poll(2)][4], [read(2)][2], [recv(2)][12], [restart_syscall(2)][13],  
[send(2)][14], [sigprocmask(2)][5], [write(2)][3], [epoll(7)][15], [time(7)][16]  

有关讨论和示例的教程，参见 [select_tut(2)][17]。

### COLOPHON  版权页     [<span class="top-link">top</span>](#top_of_page)

```
       This page is part of release 4.09 of the Linux man-pages project.  A
       description of the project, information about reporting bugs, and the
       latest version of this page, can be found at
       https://www.kernel.org/doc/man-pages/.

Linux                            2016-03-15                        SELECT(2)
```

--------------------------------------------------------------------------------

译者：[robot527](https://github.com/robot527)

[1]:http://man7.org/linux/man-pages/man7/feature_test_macros.7.html
[2]:http://man7.org/linux/man-pages/man2/read.2.html
[3]:http://man7.org/linux/man-pages/man2/write.2.html
[4]:http://man7.org/linux/man-pages/man2/poll.2.html
[5]:http://man7.org/linux/man-pages/man2/sigprocmask.2.html
[6]:http://man7.org/linux/man-pages/man7/signal.7.html
[7]:http://man7.org/linux/man-pages/man2/getrlimit.2.html
[8]:http://cr.yp.to/docs/selfpipe.html
[9]:http://man7.org/linux/man-pages/man2/accept.2.html
[10]:http://man7.org/linux/man-pages/man2/connect.2.html
[11]:http://man7.org/linux/man-pages/man3/errno.3.html
[12]:http://man7.org/linux/man-pages/man2/recv.2.html
[13]:http://man7.org/linux/man-pages/man2/restart_syscall.2.html
[14]:http://man7.org/linux/man-pages/man2/send.2.html
[15]:http://man7.org/linux/man-pages/man7/epoll.7.html
[16]:http://man7.org/linux/man-pages/man7/time.7.html
[17]:http://man7.org/linux/man-pages/man2/select_tut.2.html
