#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/select.h>

#define SERV_PORT 9300
#define LINE_MAX_LEN 2048
#define LISTENQ 20


int main(int argc, char **argv)
{
	int i, maxi, maxfd, listenfd, connfd, sockfd;
	int nready, client[FD_SETSIZE];
	ssize_t n, ret;
	fd_set rset, allset;
	char buf[LINE_MAX_LEN];
	socklen_t clilen;
	struct sockaddr_in servaddr, cliaddr;

	/* step 1: 创建一个监听套接字描述符 */
	listenfd = socket(AF_INET, SOCK_STREAM, 0);

	/* step 2: 绑定套接字 */
	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(SERV_PORT);

	bind(listenfd, (struct sockaddr *)&servaddr, sizeof(servaddr));

	/* step 3: 监听 */
	listen(listenfd, LISTENQ);

	/* step 4: 操作文件描述符集合，初始化 */
	maxfd = listenfd;
	maxi = -1;/* index to array client[] */
	for(i = 0; i < FD_SETSIZE; i++)
	{
		client[i] = -1;/* -1 indicates available entry */
	}
	FD_ZERO(&allset);
	FD_SET(listenfd, &allset);

	/* step 5: 进入服务器接收请求死循环 */
	while(1)
	{
		rset = allset;
		nready = select(maxfd + 1, &rset, NULL, NULL, NULL);
		
		if(FD_ISSET(listenfd, &rset))
		{/* new client connection */
			/* 接收客户端的请求 */
			clilen = sizeof(cliaddr);
			printf("\naccpet connection...\n");
			if((connfd = accept(listenfd, (struct sockaddr *)&cliaddr, &clilen)) < 0)
			{
				perror("accept error.\n");
				exit(1);
			}	
			printf("accpeted a new client: %s:%d\n", inet_ntoa(cliaddr.sin_addr),
					ntohs(cliaddr.sin_port));

			/* 将客户连接套接字描述符添加到数组 */
			for(i = 0; i < FD_SETSIZE; i++)
			{
				if(client[i] < 0)
				{
					client[i] = connfd;/* save descriptor */
					break;
				}
			}

			if(FD_SETSIZE == i)
			{
				perror("too many connection.\n");
				exit(1);
			}

			FD_SET(connfd, &allset);/* add new descriptor to set */
			if(connfd > maxfd)
				maxfd = connfd;/* for select */
			if(i > maxi)
				maxi = i;/* max index in client[] array */

			if(--nready < 0)
				continue;/* no more readable descriptors */
		}

		for(i = 0; i <= maxi; i++)
		{/* check all clients for data */
			if((sockfd = client[i]) < 0)
				continue;
			if(FD_ISSET(sockfd, &rset))
			{
				/* 处理客户请求 */
				printf("\nreading the socket...\n");
				
				bzero(buf, LINE_MAX_LEN);
				if((n = read(sockfd, buf, LINE_MAX_LEN)) == 0)
				{/* connection closed by client */
					close(sockfd);
					printf("close fd %d\n", sockfd);
					FD_CLR(sockfd, &allset);
					client[i] = -1;
				}
				else
				{
					printf("clint[%d] send message: %s\n", i, buf);
					if((ret = write(sockfd, buf, n)) != n)	
					{
						printf("error: write to the sockfd %d ret(%d)!\n",
							sockfd, ret);
						break;
					}
				}
				if(--nready <= 0)
					break;/* no more readable descriptors */
			}
		}
	}
}
