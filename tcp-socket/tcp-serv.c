#include "tcp-sock.h"

void str_echo(int sockfd);

int main(int argc, char **argv)
{
	struct sockaddr_in servaddr, cliaddr; /*声明服务器地址和客户链接地址*/
	socklen_t clilen;
	int listenfd, connfd; /*声明服务器监听套接字和客户端链接套接字*/
	pid_t childpid;
	char ipv4_str[16];

	/* step 1: 创建一个监听套接字描述符 listenfd */
	listenfd = socket(AF_INET, SOCK_STREAM, 0);
	if(listenfd < 0)
	{
		perror("socket created error");
		exit(1);
	}

	/* step 2: 设置服务器 sockaddr_in 结构 */
	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(SERV_PORT);

	/* step 3: 把地址赋予套接字 */
	if(bind(listenfd, (struct sockaddr*)&servaddr, sizeof(servaddr)) < 0)
	{
		perror("bind error");
		exit(1);
	}

	/* step 4: 监听套接字上的连接 */
	if(listen(listenfd, LISTENQ) < 0)
	{
		perror("listen error");
		exit(1);
	}
	printf("Waiting for connection...\n");

	/* step 5: 接受并处理客户请求 */
	for( ; ; )
	{
		clilen = sizeof(cliaddr);
		connfd = accept(listenfd, (struct sockaddr *)&cliaddr, &clilen);
		if(connfd < 0)
		{
			perror("accept error");
			exit(1);
		}
		bzero(ipv4_str, sizeof(ipv4_str));
		inet_ntop(AF_INET, &cliaddr.sin_addr.s_addr, ipv4_str, sizeof(ipv4_str));
		printf("Accepted new connection from %s:%d...\n", ipv4_str,
				ntohs(cliaddr.sin_port));

		/* 新建子进程单独处理连接 */
		if((childpid = fork()) == 0)
		{
			close(listenfd); /* 子进程关闭监听的套接字 */
			str_echo(connfd);
			exit(0);
		}
		close(connfd); /* 父进程关闭连接的套接字 */
	}

	/* step 6: 关闭监听套接字 */
	close(listenfd);
}

void str_echo(int sockfd)
{
	ssize_t n;
	char    buf[LINE_MAX_LEN];

	for( ; ; )
	{
		n = read(sockfd, buf, LINE_MAX_LEN);
		if(n > 0)
		{
			printf("read %d bytes\n", n);
			write(sockfd, buf, n);
		}
		else if(n == 0)
		{
			printf("break at %s\n", __FUNCTION__);
			break;
		}
		else
		{
			if(errno != EINTR)
				perror("str_echo: read error");
		}
	}
}
