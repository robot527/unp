#include "tcp-sock.h"

void str_cli(FILE *fp, int sockfd);

int main(int argc, char ** argv)
{
	int sockfd;
	struct sockaddr_in servaddr;

	if(argc != 2)
	{
		printf("usage: %s <A.B.C.D>\n", argv[0]);
		exit(1);
	}

	/* step 1: 创建套接字 */
	if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		perror("socket error");
		exit(1);
	}

	/* step 2: 设置连接服务器地址结构 */
	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(SERV_PORT);
	if(inet_pton(AF_INET, argv[1], &servaddr.sin_addr) < 0)
	{
		printf("inet_pton error for %s\n", argv[1]);
		exit(1);
	}

	/* step 3: 发送连接服务器请求 */
	if(connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0)
	{
		perror("connected error");
		exit(1);
	}

	/* step 4: 发送文本，输出回显 */
	str_cli(stdin, sockfd);

	/* step 5: 关闭套接字 */
	close(sockfd);

	return 0;
}

/* readline 函数的简单实现，仅作示例 */
ssize_t readline(int fd, void *vptr, size_t maxlen)
{
	ssize_t	n, rc;
	char	c, *ptr;

	ptr = vptr;
	for(n = 1; n < maxlen; n++)
	{
		if((rc = read(fd, &c, 1)) == 1)
		{
			*ptr++ = c;
			if(c == '\n')
				break;	/* newline is stored, like fgets() */
		}
		else if(rc == 0)
		{
			*ptr = 0;
			return (n - 1);	/* EOF, n - 1 bytes were read */
		}
		else
		{
			if(errno != EINTR)
				return -1;		/* error, errno set by read() */
		}
	}

	*ptr = 0;	/* null terminate like fgets() */
	return n;
}

/* str_cli: reads a line of text from standard input,
 * writes it to the server, reads back the server's
 * echo of the line, and outputs the echoed line to
 * standard output.
 */
void str_cli(FILE *fp, int sockfd)
{
	char sendline[LINE_MAX_LEN], recvline[LINE_MAX_LEN];

	while(fgets(sendline, LINE_MAX_LEN, fp) != NULL)
	{
		write(sockfd, sendline, strlen(sendline));

		if(readline(sockfd, recvline, LINE_MAX_LEN) == 0)
		{
			perror("server terminated prematurely");
			break;
		}

		if(fputs(recvline, stdout) == EOF)
		{
			perror("fputs error");
			break;
		}
	}
}

