#include <stdio.h> /* printf(), perror() */
#include <stdlib.h> /* exit() */
#include <string.h> /* bzero() */
#include <errno.h>
#include <sys/socket.h> /* basic socket definitions */
#include <netinet/in.h> /* sockaddr_in{} and other Internet defns */
#include <unistd.h> /* fork(), close(), read(), write() */
#include <arpa/inet.h> /* inet_pton(), inet_ntop() */

#define SERV_PORT     9300
#define SERV_PORT_STR "9300"
#define LISTENQ       1024 /* 2nd argument to listen() */
#define LINE_MAX_LEN  2048

